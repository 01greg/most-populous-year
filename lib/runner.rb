require 'CSV'
require_relative './most_populous_year'

imported_csv = File.read(ARGV.first)
imported_parsed = CSV.parse(imported_csv).to_a

mpy = MostPopulousYear.new(imported_parsed)
mpy.run
