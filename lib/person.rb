class Person
  attr_reader :life_span
  # Creates a new Person object.
  #
  # @birth_year [Integer] The year of birth.
  # @end_year [Integer] The year of last record.
  def initialize(birth_year, end_year)
    @birth_year = birth_year.to_i
    @end_year = end_year.to_i
    @life_span = (@birth_year...@end_year).to_a
  end

  # Checks the life years.
  #
  # @return [Boolean] returns true if birth years are valid or false if otherwise
  def valid?
    @birth_year.is_a?(Integer) && @birth_year.between?(1900,2000) &&
      @end_year.is_a?(Integer) && @end_year.between?(1900,2000) && @birth_year <= @end_year
  end
end
