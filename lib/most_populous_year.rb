require_relative './person'

class MostPopulousYear
    def initialize(imported_data)
      @people = imported_data.map do |person|
        Person.new(person[0], person[1])
      end
    end

    # Validates calculates and prints the results.
    def run
      validate_data
      calculate_data
      print_data
    end

    private

    # Validates the persons life range terminates on error.
    def validate_data
      @people.each do |person|
        if !person.valid?
          puts 'invalid data...exiting.'
          exit
        end
      end
    end

    # Flattens all the people/years into one array.
    def calculate_data
      all_years = @people.map(&:life_span).flatten

      years_hash = count(all_years)

      sorted_hash = years_hash.sort_by do |_key, value|
        value
      end

      sorted_hash = sorted_hash.reverse.to_h

      @greatest_values = solve_ties(sorted_hash)
    end

    # Counts the instances of each year in the flattened year array.
    #
    # @param [Array] years the array of all flattened years for every persons lifespan
    # @return [Hash] year and count of instances
    def count(years)
      years_with_count = {}

      years.each do |year|
        years_with_count[year] ||= 0
        years_with_count[year] += 1
      end

      years_with_count
    end

    # Iterates and prints the results.
    def print_data
      @greatest_values.each do |year|
        puts year
      end
    end

    # Ckecks for case when multiple years appear with the same greatest value.
    #
    # @param [Hash] hash of all of the years and counts
    # @preturn [Array] array with the years that have greatest value
    def solve_ties(hash)
      greatest_value = hash.values.first

      years = hash.map do |year, count|
        year if count == greatest_value
      end

      years.compact
    end
end
