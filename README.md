# Most Populous Year

# Description

 - Given a list of people with their birth and end years (all between 1900 and 2000), find the year with the most number of people alive.


# Run Instructions

 - `ruby runner.rb ../data/data.csv`



# Assumptions

 - The "end year" is not included in the count of being alive.
